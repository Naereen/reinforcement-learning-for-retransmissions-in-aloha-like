<!--
$theme: default
$size: 4:3
page_number: true
footer: « Meeting @ SCEE Team » | 20 September 2018 | By: Rémi, Julio & Lilian
-->

<link rel="stylesheet" type="text/css" href="css/marp-naereen.css" />

# Meeting @ SCEE Team
> CentraleSupélec, campus of Rennes

- With who:
  - Rémi Bonnefoi,
  - Julio Mango Vasquez,
  - Lilian Besson,
- And with:
  - Christophe Moy,
  - Carlos Faouzi Bader
- :calendar: 20 September 2018

---

# Agenda for today :watch:

1. Presentation of what we already did, who and when
2. Presentation of what is still to be done
3. Dates and calendar:
   + who can help when and for what
   + when to send the article
   + ($+$ deadline extension?)
4. Questions and discussions

> Maximum 1 hour meeting

---

# 1. What we already did :ok_hand:

> Overview :eye:

## 1.1. About the problem
## 1.2. Ideas
## 1.3. Model
## 1.4. Computations
## 1.5. Simulations

---

# 1.1. About the problem

## Context
- An IoT network, one base station, $K$ channels, many objects
- Static objects ("background") *vs* dynamic objects (that we control)
- Slotted ALOHA protocol:
  + Device has probability $p$ to send in a slot $t$ (first transmission)
  + If collision, wait a random time $\sim [0,\ldots,m-1]$ (back-off)
- Maximum number of transmission $M_t \in\mathbb{N}^*$ of packet at time $t$
  (before being considered lost)

---

## Context
- If machine learning algorithms are used to select the channel
  for the first transmission of each message…
  > We can denote $A^j(t)\in[1,\ldots,K]$ for object $\#j$ at time $t$.
  
- :boom: Then maybe the retransmission behavior will mess up the learning process, and maybe the object can be smarter !?

## Why?
- the goal of learning (UCB etc) is to identify the best channel 
  (*i.e.*, the most free, in terms of static background traffic)
- when all objects have learned this…
- they will all use the same channel $\Rightarrow$ lots of collisions!

---

# 1.2. Ideas

- We want to study the possible effect of non smart or smart retransmission on the global network efficiency
  > Measure as the *successful transmission rate*.

- Propose heuristics
- Compare them

---

# 1.2. Ideas

We want to compare different approaches:

1. the naive (non learning) approach:
   + select $A^j(t)$ uniformly at random, for first transmission
   + and keep the same for retransmissions
2. a naive (learning) approach:
   + use a Multi-Armed Bandit algorithm (*e.g.*, UCB, Thompson Sampling) to learn how to select $A^j(t)$, for first transmission
   + and keep the same for retransmissions

3. non naive approaches:
   + use a more complex learning procedure

---

# Heuristics
## *Delayed UCB*
- One UCB learns for all transmissions, but gets the feedback only when message was succesfully sent or if it failed after $M_t$ trials

## *Two UCB*
- One UCB learns for $1^{\text{st}}$ transmission, another for next ones

---

# Heuristics

## $K+1$ *UCB*
- One UCB learns for $1^{\text{st}}$ transmission, and when transmitting in channel $k$, one of the $K$ other UCB algorithm is used to learn for next transmissions (slower but could be better?)

> *Note:* UCB could be any algorithm here (Thompson Sampling etc)

---

# 1.3. Model: Markov chain (one object)

![bg original 75%](Markov_model.png)

---

# 1.3. Model

- $p_c$ is proba $\mathbb{P}$ of collision at first transmission
- $p_{c1}$ is $\mathbb{P}$ of collision at second transmission (and so on)

$\Rightarrow$ a first goal can be to approximate $p_{c1}$ :smiley:

---

## Hypotheses (H1)
- The probability to have a collision at the second transmission $p_{c1}$
- can be decomposed into the probability of having a collision with a packet transmitted by devices involved in the collision at the first transmission, which is denoted $p_{ca}$,
- and the probability of having a collision with a packet transmitted by other devices, not involved in the previous collision.

- The number of devices involved in the previous collision is supposed to be small enough, compared to the number of devices in the channel, to consider that this second probability is equal to $p_c$.

---

## Hypotheses (H2)

- $M_t$ is supposed to be large enough to consider that devices are hardly ever in the last state.

- With this hypothesis, if a device is involved in the previous collision it retransmits its packet after a random back-off time.

---

# 1.4. Computations

- Assume a steady state of the Markov chain, and on object.
Let $x_t^i$ be the $\mathbb{P}$ that it is transmistting a packet
  for the $i+1$ time in a given time slot $t$
- Let $x_t := \sum\limits_{i=0}^{M_t - 1} x_t^i$ the $\mathbb{P}$ of transmitting in slot $t\in\mathbb{N}$
- The $\mathbb{P}$ of collision at first transmission
  $$p_c = 1-\left(1-x_t\right)^{N-1} \Longleftrightarrow x_t = 1-\left(1-p_c\right)^{\frac{1}{N-1}}.$$
- And the $\mathbb{P}$ of collision with $k$ different packets, $p_c(k)$ is
  $$p_c(k) = {N-1 \choose k} x_t^k \left(1-x_t\right)^{N-1-k}.$$

---

# 1.4. Computations

- $p_{c1} = p_{ca}+\left(1-p_{ca} \right)p_c$ by Hypothesis H1
- $p_{ca} = \sum\limits_{k=1}^{N-1}p_{ca}(k)$ by Hypothesis H2

$$p_{ca} = 1- \frac{1}{p_c}\sum_{k=1}^{N-1}{N-1 \choose k} x_t^k \left(1-x_t\right)^{N-1-k}\left( 1-\frac{1}{m}\right)^k$$
$$= 1- \frac{\left(1-x_t\right)^{N-1}}{p_c}\sum_{k=1}^{N-1}{N-1 \choose k} x_t^k \left( 1-\frac{1}{m}\right)^k.$$

- We can approximate $N - 1 - k \simeq N - 1$ for first terms ($k \ll N$)

---

# 1.4. Computations

$$ p_{ca} = 1- \frac{1}{p_c}\sum_{k=1}^{N-1}{N-1 \choose k} x_t^k \left(1-x_t\right)^{N-1-k}\left( 1-\frac{1}{m}\right)^k$$
$$= 1- \frac{\left(1-x_t\right)^{N-1}}{p_c}\sum_{k=1}^{N-1}{N-1 \choose k} x_t^k \left( 1-\frac{1}{m}\right)^k$$
$$= \frac{1}{p_c}-\left(\frac{1}{p_c}-1\right)\left[ 1+\left(1-\left(1-p_c\right)^{\frac{1}{N-1}}\right)\left(1-\frac{1}{m}\right)\right]^{N-1}.$$

$\Rightarrow$ we can compute $p_{c1}$ numerically. :ok_hand:

> :clap: Rémi

---

# 1.5. Simulations

- We want to check the approximation derived by Rémi
- We also want to simulate a (relatively small) network
  + with $K$ channels (small)
  + with lots of dynamic objects
  + with a static (stationary) background traffic (*i.i.d.*)
  + with different algorithms and heuristics

---

# 1.5. Simulation code

## What
- In MATLAB (probably compatible with GNU Octave…)
- 13 files
	- Painly written by Julio and helped by Rémi :clap:
	- Proofread by Lilian :pencil:
- Already about 2300 lines!

## Where?
- Already hosted online
  [`https://bitbucket.org/scee_ietr/ucb_smart_retrans`](https://bitbucket.org/scee_ietr/ucb_smart_retrans)
- Not yet public, but can be made open source when we want

---

# 1.5. Overview of the simulation code

```bash
$ cd ~/these/src/ucb_smart_retrans.git
$ wc -l *.m
   15 TrafficDevices.m
   20 TrafficRTxDevices.m
   65 tx_with_retransmissions.m
  107 Effect_retransm.m
  122 Approx_effect_retransm.m
  246 Random_selection_Fram.m
  178 UCB_Slotted_IoT_Retransmissions.m
  283 UCB_Slotted_IoT_Retransmissions_UCB_contextual.m
  258 UCB_Slotted_IoT_Retransmissions_UCBRand.m
  290 UCB_Slotted_IoT_Retransmissions_UCB_UCB2_delayed.m
  283 UCB_Slotted_IoT_Retransmissions_UCB_UCB2.m
  264 UCB_Slotted_IoT_Retransmissions_Fram.m
  264 TS_Slotted_IoT_Retransmissions_Fram.m
 2395 total
```

> :clap: Julio

---

# 1.5. Simulation results

## Let see some plots!

---

# Approximation of $p_{c_1}$
- For this example: $p=10^{-3}$, $\forall t, M_t = 10$, $m=10$

> ![110%](Approximation_m10.png)

- Approximation is precise where $p_{c1} \leq 50\%$
- Gap betweenn $p_c$ and $p_{c1}$ can be large: about $10\%$, so bang smart about retransmission could really help!


---

# First simulation
![bg original 70%](comparison_100300300300.jpg)

---

# Large simulation
![bg original 70%](UCBtrial.jpg)

---

# Conclusions from simulation

## :smiley:
- The approximation of $p_{c_1}$ seems correct and accurate
- We can easily add new heuristics and compare them

## :cry:
- So far, a simple UCB is more efficient than other ideas
  (but research is not only about "positive" results!)
- Simulations take some time

---

# 2. What is still to be done :boom:

## In the next few days
- Finish to write most of the paper
- Proofread (model, maths, simulations results)
- Write a good introduction, state-of-the-art, conclusion
 
$\Rightarrow$ Send a first version **on next Tuesday**!

## Next weeks (after deadline extension) 
- More experiments
- More plots

---

# 3. Dates and calendar :calendar:
## 3.1. Who can help, when and for what?
## 3.2. When to send the article?

---

# 3.1. Who can help, when and for what?

- Today and tomorrow: write as most of we can (Julio, Lilian)
- This weekend: finish (Lilian)
- Next Monday:
  + someone should proofread the paper (Christophe and/or Carlos)

---

# 3.2. When to send the article

- :warning: **Tuesday 25th of September**

- $+$ deadline extension?
  :ok: We can count on it (I think)
  :warning: but we have to send a first version before!

- After deadline extension, we can:
  + clean up maths and presentation
  + improve experiments and plots
  + a better introduction and state of the art? 

---

# 4. Questions and discussions

## Questions
- Something not clear?
- Something can be improved?
- Anything else?

## Discussions…

---

# Thanks for today! :pray:

> Slides by Lilian